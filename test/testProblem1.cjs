const createAndDeleteFiles = require('../problem1.cjs');
const path = require('path');

const pathDirectory = path.resolve(__dirname, './random');

const randomNumber = 10;

createAndDeleteFiles(randomNumber, pathDirectory);