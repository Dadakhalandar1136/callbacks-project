const fs = require('fs');

const createAndDeleteFiles = function (creatingFiles, filesFolder) {
    fs.mkdir(filesFolder, (error) => {
        if (error) {
            throw error;
        } else {
            console.log("Directory created succefully");
        }

        const randomFiles = Math.round(Math.random() * creatingFiles);

        for (let index = 0; index < randomFiles; index++) {
            fs.writeFile(`${filesFolder}/file${[index]}.json`, JSON.stringify({ name: 'Dadu', age: '25' }), (error) => {
                if (error) {
                    console.error(error);
                } else {
                    console.log('File created successfully');
                    fs.unlink(`${filesFolder}/file${index}.json`, (error) => {
                        if (error) {
                            console.error(error);
                        } else {
                            console.log('File deleted from random directory');
                        }
                    });
                }
            });
        }
    });
}

module.exports = createAndDeleteFiles;

