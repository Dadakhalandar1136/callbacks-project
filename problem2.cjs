const fs = require('fs');

const multipleScenarios = function (filePath, appendFilePath) {
    fs.readFile(filePath, 'utf8', (error, data) => {
        if (error) {
            console.error(error);
        } else {
            let dataToUpperCase = data.toUpperCase();
            fs.writeFile('to-upper-case.txt', dataToUpperCase, (error) => {
                if (error) {
                    console.error(error);
                } else {
                    fs.appendFile(appendFilePath, 'to-upper-case.txt\n', (error) => {
                        if (error) {
                            console.error(error);
                        } else {
                            console.log('Text converted to upper case and stored the filename into new file');
                        }
                    });
                    fs.readFile('to-upper-case.txt', 'utf8', (error, data) => {
                        if (error) {
                            console.error(error);
                        } else {
                            let dataToLowerCase = data.toLowerCase();
                            let splitSentences = dataToLowerCase.split('.').join('.\n');
                            fs.writeFile('to-lower-case.txt', splitSentences, (error) => {
                                if (error) {
                                    console.error(error);
                                } else {
                                    fs.appendFile(appendFilePath, 'to-lower-case.txt\n', (error) => {
                                        if (error) {
                                            console.error(error);
                                        } else {
                                            console.log('Text converted to lower case and splitted into sentences and stored the filename into new file');
                                        }
                                    });
                                    fs.readFile('to-lower-case.txt', 'utf8', (error, data) => {
                                        if (error) {
                                            console.error(error);
                                        } else {
                                            let textSort = data.split('\n').sort();
                                            fs.writeFile('sorted-file.txt', textSort.join('\n').toString(), (error) => {
                                                if (error) {
                                                    console.error(error);
                                                } else {
                                                    fs.appendFile(appendFilePath, 'sorted-file.txt', (error) => {
                                                        if (error) {
                                                            console.error(error);
                                                        } else {
                                                            console.log('Sorted the content of new file and store into new filenames');
                                                        }
                                                    });
                                                    fs.readFile(appendFilePath, 'utf8', (error, data) => {
                                                        if (error) {
                                                            console.error(error);
                                                        } else {
                                                            console.log(data);
                                                            dataToSplit = data.split('\n');

                                                            for (let index = 0; index < dataToSplit.length; index++) {
                                                                fs.unlink(`./${dataToSplit[index]}`, (error) => {
                                                                    if (error) {
                                                                        console.error(error);
                                                                    } else {
                                                                        console.log('File names deleted');
                                                                    }
                                                                });
                                                            }
                                                        }
                                                    });
                                                }
                                            });
                                        }
                                    });
                                }
                            });
                        }
                    });
                }
            });
        }

    })
}

module.exports = multipleScenarios;